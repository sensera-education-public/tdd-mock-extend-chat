package com.example.tddchat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class ChatServerTest {

    ChatServer chatServer;
    UserList userList;

    @BeforeEach
    void setUp() {
        userList = mock(UserList.class);
        chatServer = new ChatServer(userList);
    }

    @Test
    void test_register_username_success() {
        // When
        Output output = mock(Output.class);
        chatServer.requesterUsername("Arne", output);

        // Then
        verify(userList, times(1)).add(eq("Arne"), eq(output));
    }

    @Test
    void test_connect_success() {
        // Given
        Output output = mock(Output.class);
        User user = new User("Arne", output);
        when(userList.getUser("Agda")).thenReturn(user);

        // When
        Link link = chatServer.connect("Agda");

        // Then
        assertNotNull(link);
    }

}
