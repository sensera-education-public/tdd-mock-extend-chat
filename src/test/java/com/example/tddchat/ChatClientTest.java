package com.example.tddchat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class ChatClientTest {

    private Output output;
    private Input input;
    private ChatServer chatServer;
    private ChatClient chatClient;
    private Link link;

    @BeforeEach
    void setUp() {
        output = mock(Output.class);
        input = mock(Input.class);
        chatServer = mock(ChatServer.class);
        chatClient = new ChatClient(output, input, chatServer);
        link = mock(Link.class);
        when(chatServer.connect(eq(null))).thenReturn(link);
    }

    @Test
    void test_run_chat_system_success() {
        // When
        chatClient.run();

        // Then
        assertEquals(ChatClient.Status.INPUT_NAME, chatClient.getStatus());
        verify(output, times(1)).print(eq("Enter your name:"));
    }

    @Test
    void test_input_name_chat_system_success() {
        // Given
        when(input.getCommand()).thenReturn("Arne", null, null);

        // When
        chatClient.run();

        // Then
        assertEquals(ChatClient.Status.INPUT_NAME, chatClient.getStatus());
        assertEquals("Arne", chatClient.getUserName());
        verify(output, times(1)).print(eq("Enter your name:"));
        verify(chatServer, times(1)).requesterUsername(eq("Arne"),eq(output));
    }

    @Test
    void test_input_name_of_member_success() {
        // Given
        ArgumentCaptor<String> argument = ArgumentCaptor.forClass(String.class);
        when(input.getCommand()).thenReturn("Arne", "Agda", null);
        when(chatServer.connect(eq("Agda"))).thenReturn(link);

        // When
        chatClient.run();

        // Then
        assertEquals(ChatClient.Status.INPUT_NAME, chatClient.getStatus());
        assertEquals("Arne", chatClient.getUserName());
        assertEquals("Agda", chatClient.getMember());
        verify(output, times(2)).print(argument.capture());
        assertEquals(List.of("Enter your name:", "Enter name of chat member:"), argument.getAllValues());
    }

    @Test
    void test_chat_with__member_success() {
        // Given
        ArgumentCaptor<String> argument = ArgumentCaptor.forClass(String.class);
        when(input.getCommand()).thenReturn("Arne", "Agda", "Hello", null);
        when(chatServer.connect(eq("Agda"))).thenReturn(link);

        // When
        chatClient.run();

        // Then
        verify(output, times(2)).print(argument.capture());
        assertEquals(List.of("Enter your name:", "Enter name of chat member:"), argument.getAllValues());
        verify(link, times(1)).send(eq("Hello"));
    }
}
