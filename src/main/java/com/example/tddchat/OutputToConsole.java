package com.example.tddchat;

public class OutputToConsole implements Output {
    String prefix;

    public OutputToConsole(String prefix) {
        this.prefix = prefix;
    }

    public void print(String msg) {
        System.out.println(prefix+msg);
    }
}
