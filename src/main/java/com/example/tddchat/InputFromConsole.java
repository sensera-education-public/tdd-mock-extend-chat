package com.example.tddchat;

import java.util.Scanner;

public class InputFromConsole implements Input{
    Scanner in = new Scanner(System.in);

    public String getCommand() {
        return in.next();
    }
}
