package com.example.tddchat;

public class Link {
    private Output output;

    public Link(Output output) {
        this.output = output;
    }

    public void send(String msg) {
        output.print(msg);
    }
}
