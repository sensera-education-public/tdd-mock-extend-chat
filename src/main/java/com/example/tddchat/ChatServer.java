package com.example.tddchat;

public class ChatServer {

    private UserList userList;

    public ChatServer(UserList userList) {
        this.userList = userList;
    }

    Link connect(String name) {
        User user = userList.getUser(name);
        return new Link(user.getOutput());
    }

    public void requesterUsername(String name, Output output) {
        userList.add(name, output);
    }

}
