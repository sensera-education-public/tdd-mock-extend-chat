package com.example.tddchat;

public class MainChat {


    public static void main(String[] args) {
        ChatServer chatServer = new ChatServer(new UserList());

        ChatBot chatBot = new ChatBot("Agda: ");

        Output arneOutput = new OutputToConsole("Arne: ");

        chatServer.requesterUsername("Arne", arneOutput);
        chatServer.requesterUsername("Agda", chatBot);

        chatBot.connect(chatServer.connect("Arne"));

        Link arneLinkToAgda = chatServer.connect("Agda");

        arneLinkToAgda.send("Hello");
        arneLinkToAgda.send("How old are you Agda?");
    }
}
