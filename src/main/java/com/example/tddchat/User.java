package com.example.tddchat;

import lombok.Value;

@Value
public class User {
    String userName;
    Output output;

    public User(String userName, Output output) {
        this.userName = userName;
        this.output = output;
    }
}
