package com.example.tddchat;

class ChatBot implements Output {
    String name;
    Link link;

    public ChatBot(String name) {
        this.name = name;
    }

    void connect(Link link) {
        this.link = link;
    }

    @Override
    public void print(String msg) {
        System.out.println("Bot: " + msg);
        if (msg.equals("Hello"))
            link.send("Hi");
        if (msg.equals("How old are you Agda?"))
            link.send("199");
    }
}
