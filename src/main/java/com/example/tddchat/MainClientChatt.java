package com.example.tddchat;

public class MainClientChatt {
    public static void main(String[] args) throws InterruptedException {
        ChatServer chatServer = new ChatServer(new UserList());

        ChatBot chatBot = new ChatBot("Agda: ");
        chatServer.requesterUsername("Agda", chatBot);

        new Thread(() -> {
            while (true) {
                try {
                    chatBot.connect(chatServer.connect("Arne"));
                } catch (Exception e) {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException ex) {
                        ex.printStackTrace();
                    }
                }
            }

        }).start();

        ChatClient chatClient = new ChatClient(new OutputToConsole(""), new InputFromConsole(), chatServer);
        chatClient.run();

        //Output arneOutput = new OutputToConsole("Arne: ");

        //chatServer.requesterUsername("Arne", arneOutput);





    }
}
