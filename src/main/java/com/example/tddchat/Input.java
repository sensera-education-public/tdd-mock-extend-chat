package com.example.tddchat;

import java.util.Scanner;

public interface Input {
    String getCommand() ;
}
