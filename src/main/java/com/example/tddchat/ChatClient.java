package com.example.tddchat;

import lombok.Data;

@Data
public class ChatClient {

    enum Status {
        INPUT_NAME,
        CONNECT,
    }

    private Output output;
    private Input input;
    private ChatServer chatServer;
    private Status status;
    private String userName;
    private String member;

    public ChatClient(Output output, Input input, ChatServer chatServer) {
        this.output = output;
        this.input = input;
        this.chatServer = chatServer;
        this.status = Status.INPUT_NAME;
    }

    public void run() {
        output.print("Enter your name:");
        userName = input.getCommand();
        chatServer.requesterUsername(userName, output);
        output.print("Enter name of chat member:");
        member = input.getCommand();
        Link link = chatServer.connect(member);

        while (true) {
            String msg = input.getCommand();
            if (msg == null || msg.equals(""))
                return;
            link.send(msg);
        }
    }
}
