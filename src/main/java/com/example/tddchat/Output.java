package com.example.tddchat;

public interface Output {

    void print(String msg);
}
