package com.example.tddchat;

import java.util.ArrayList;
import java.util.List;

public class UserList {
    List<User> users = new ArrayList<>();

    public void add(String name, Output output) {
        users.add(new User(name, output));
    }

    public User getUser(String name) {
        return users.stream()
                .filter(s -> s.getUserName().equals(name))
                .findAny()
                .orElseThrow(()-> new RuntimeException("User not found "+name));
    }
}
